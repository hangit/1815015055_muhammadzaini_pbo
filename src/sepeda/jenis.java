
package sepeda;

public class jenis {
        private String jenis;
        private String warna;
         private String harga;
          private int ukuran;

    public String getWarna() {
        return warna;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public int getUkuran() {
        return ukuran;
    }

    public void setUkuran(int ukuran) {
        this.ukuran = ukuran;
    }

    public String getJenis() {
        return jenis;
        
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }
        
}